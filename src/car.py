import arcade
import numpy as np


class Car:

    def __init__(self, x, y, limits, color, size, speed, acceleration):
        self.x = x
        self.y = y
        self.limits = limits
        self.isAlive = True
        self.color = color
        self.score = 0
        self.size = size
        self.speed = speed
        self.acceleration = acceleration
        self.turn = 0


    def move(self, xx, yy):
        self.x += xx
        self.y += yy
        if self.limits[self.y, self.x] == 0:
            self.isAlive = False


def update_cars(cars):
    for car in cars:
        if car.isAlive:
            frame_time = 1
            distance = int(car.speed * frame_time + car.acceleration * frame_time ** 2 / 2)
            car.speed = frame_time * car.acceleration + car.speed

            car.move(np.random.randint(90 * car.turn + 1), distance)

def init_cars(nb_cars, x, y, limits, size, speed, acceleration):
    max_cars = 10
    if nb_cars > max_cars:
        nb_cars = max_cars
    cars = []
    colors = get_car_colors() 
    for i in range(nb_cars):
        cars.append(Car(x, y, limits, colors[i], size, speed, acceleration))
    return cars


def get_car_colors():
    colors = [
        arcade.color.AFRICAN_VIOLET,
        arcade.color.ALIZARIN_CRIMSON,
        arcade.color.SAE,
        arcade.color.ANDROID_GREEN,
        arcade.color.ANTIQUE_BRONZE,
        arcade.color.AUREOLIN,
        arcade.color.BABY_PINK,
        arcade.color.BALL_BLUE,
        arcade.color.AVOCADO,
        arcade.color.BROWN_NOSE
    ]

    return colors

