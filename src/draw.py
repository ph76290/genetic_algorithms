import arcade


def draw_cars(cars):
    for car in cars:
        if car.isAlive:
            print("drawing car: {} & {}".format(car.x, car.y))
        arcade.draw_rectangle_filled(
            car.x,
            car.y,
            car.size,
            car.size,
            car.color
        )


def draw_walls(walls, road_width):

    for i in range(len(walls) - 1):

        arcade.draw_line(
                walls[i][0],
                walls[i][1],
                walls[i + 1][0],
                walls[i + 1][1],
                arcade.color.BLACK, 2)
        arcade.draw_line(
                walls[i][0] + road_width,
                walls[i][1],
                walls[i + 1][0] + road_width,
                walls[i + 1][1],
                arcade.color.BLACK, 2)
