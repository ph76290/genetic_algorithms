import arcade
import pyglet
import numpy as np
import os
from draw import *
from car import *
from game_state import State


def print_matrix(matrix):
    #matrix = np.flip(matrix, axis=0)
    for i in range(len(matrix)):
        print("%03d" % i, end="")
        for j in range(len(matrix[0])):
            print(int(matrix[i, j]), end="")
        print()


def generate_limits(width, height, walls, road_width):

    limits = np.zeros((height, width))

    for i in range(len(walls) - 1):

        if walls[i + 1][0] == walls[i][0]:
            limits[walls[i][1]:walls[i + 1][1], walls[i][0]:walls[i][0] + road_width] = 1
            continue

        val = walls[i + 1][0] - walls[i][0]
        a = (walls[i + 1][1] - walls[i][1]) / val
        for j in range(0, val, val // abs(val)):
            limits[walls[i][1] + int(j * a):walls[i][1] + int((abs(j) + 1) * abs(a)), j + walls[i][0]: j + walls[i][0] + road_width] = 1

    #print_matrix(limits)
    return limits


class MyArcade(arcade.Window):


    def __init__(self, width, height, title):
        super().__init__(width, height, title)
        arcade.set_background_color(arcade.color.WHITE)
        self.score = None
        self.state = State.MAIN_MENU
        self.isOver = False


    def setup(self, cars, walls, road_width):

        self.score = 0
        self.score_board = arcade.SpriteList()
        self.sprites['background'] = self.background
        self.cars = cars
        self.walls = walls
        self.road_width = road_width
        self.menus = {'start': arcade.load_texture(GET_READY_MESSAGE),
                      'gameover': arcade.load_texture(GAME_OVER),
                      'play': arcade.load_texture(PLAY_BUTTON)}

    
    def update(self, delta_time):
        arcade.start_render()

        draw_walls(self.walls, self.road_width)
        draw_cars(self.cars)
        update_cars(self.cars)


    def on_draw(self):

        # Start rendering and draw all the objects
        arcade.start_render()
        # Calling "draw()" on a SpriteList object will call it on each child in the list.

        # Whatever the state, we need to draw background, then pipes on top, then base, then bird.
        self.draw_background()
        self.walls_sprites.draw()
        self.cars.draw()

        if self.state == State.MAIN_MENU:
            # Show the main menu
            texture = self.menus['start']
            arcade.draw_texture_rectangle(self.width // 2, self.height // 2 + 50, texture.width, texture.height, texture, 0)

        elif self.state == State.PLAYING:
            # Draw the score board when the player start playing.
            self.draw_score_board()

        elif self.state == State.GAME_OVER:
            # Draw the game over menu if the player lost + draw the score board.
            texture = self.menus['gameover']
            arcade.draw_texture_rectangle(self.width // 2, self.height // 2 + 50, texture.width, texture.height, texture, 0)
            texture = self.menus['play']
            arcade.draw_texture_rectangle(self.width // 2, self.height // 2 - 100, texture.width, texture.height, texture, 0)
            self.draw_score_board()

    def on_key_release(self, symbol, modifiers):

        if symbol == arcade.key.SPACE and self.state == State.MAIN_MENU:
            # If the game is starting, just change the state and return
            self.state = State.PLAYING
            return

    def draw_score_board(self):
        self.score_board.draw()


    def build_score_board(self):
        """
        Builds the score board with images. Basically how this work:
        1. Calculate how many digits in the score.
        2. Calculate width (Number of digits * width of each digit image width)
        3. Calculate the "left" x position that makes all the images centered.
        4. Just append every digit's image in the score board.
        :return:
        """
        score_length = len(str(self.score))
        score_width = 24 * score_length
        left = (self.width - score_width) // 2
        self.score_board = arcade.SpriteList()
        for s in str(self.score):
            self.score_board.append(arcade.Sprite(SCORE[s], 1, center_x=left + 12, center_y=450))
            left += 24




    def on_update(self, delta_time):

        """
        This is the method called each frame to update objects (Like their position, angle, etc..) before drawing them.
        """
        # print(delta_time)
        # Whatever the state, update the bird animation (as in advance the animation to the next frame)

        if self.state == State.PLAYING:
            self.build_score_board()

            update_cars(self.cars)

            new_pipe = None

            # Kill pipes that are no longer shown on the screen as they're useless and live in ram and create a new pipe
            # when needed. (If the center_x of the closest pipe to the bird passed the middle of the screen)
            for wall in self.walls_sprites:
                if wall.x <= 0:
                    wall.kill()
                elif len(self.walls_sprites) == 2 and wall.right <= random.randrange(self.width // 2, self.width // 2 + 15):
                    new_pipe = Pipe.random_pipe_obstacle(self.sprites, self.height)

            if new_pipe:
                self.walls_sprites.append(new_pipe[0])
                self.walls_sprites.append(new_pipe[1])

            # This calls "update()" Method on each object in the SpriteList
            self.walls_sprites.update()
            self.bird.update(delta_time)
            self.bird_list.update()

            # If the bird passed the center of the pipe safely, count it as a point.
            # Hard coding.. :)
            if self.bird.center_x >= self.pipe_sprites[0].center_x and not self.pipe_sprites[0].scored:
                self.score += 1
                # Well, since each "obstacle" is a two pipe system, we gotta count them both as scored.
                self.pipe_sprites[0].scored = True
                self.pipe_sprites[1].scored = True
                print(self.score)

            # Check if the bird collided with any of the pipes
            hit = arcade.check_for_collision_with_list(self.bird, self.pipe_sprites)

            if any(hit):
                arcade.play_sound(SOUNDS['hit'])
                self.state = State.GAME_OVER
                self.bird.die()

        elif self.state == State.GAME_OVER:
            # We need to keep updating the bird in the game over scene so it can still "die"
            self.bird.update()
