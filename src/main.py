#!/usr/bin/env python3

from map import *
from car import *


def main():

    # Set constants for the screen size
    SCREEN_WIDTH = 60
    SCREEN_HEIGHT = 400

    walls = [(10, 0), (10, 150), (30, 300), (20, 400)]
    road_width = 20
 
    limits = generate_limits(SCREEN_WIDTH, SCREEN_HEIGHT, walls, road_width)
    size = 5
    cars = init_cars(1, 20, 2, limits, size, 0, 0.08)

    window = MyArcade(SCREEN_WIDTH, SCREEN_HEIGHT, "Genetic cars")
    window.setup(cars, walls, road_width)

    window.schedule(window.update, 1000)
    window.run()


main()
